import React, { useState } from 'react';

const Button = ({ incrementBy }) => {

    const [count, setCount] = useState(0);

    const handleClick = () => {
        setCount(count + incrementBy);
    };

    const divStyle = {
        color: 'blue',
        border: "1px solid black",
        borderWidth: "20px",
        borderRadius: "10px",
    }
    return (
        <div style={divStyle}>
            {count}
            <button onClick={handleClick}>{incrementBy}</button>
        </div>
    )
}

export default Button

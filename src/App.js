import React from 'react';
import Button from "./Button";
import "./App.css";

const App = () => {

  return (
    <div className="App">
      <Button incrementBy={2} />
      <br />
      <Button incrementBy={5} />
    </div>
  )
}

export default App
